<?php
/**
* @author    EVS Webstudio http://www.itmed.su/
* @copyright Copyright (C) EVS Webstudio
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgSystemEvs_Fontsize_Switcher extends JPlugin {
	
	public function onContentPrepare($context, &$row, &$params, $page=1) {
		
		preg_match_all('#\[fontsize\]#', $row->text, $matches);
		
		if (count($matches[0])) {
			
			// add language package
			$language = JFactory::getLanguage();
			$language->load('plg_system_evs_fontsize_switcher', JPATH_ADMINISTRATOR, null, true);
			
			$html = array();
			
			// start list
			$html[] = '<ul id="'.$this->params->get( 'default_id' ).'">';
			
			// add previous text
			if ( $this->params->get( 'show_previous_text' ) ) {
				$html[] = '<li>';
				$html[] = $this->get_text($this->params->get( 'previous_text' ), 'PREVIOUS_TEXT');
				$html[] = '</li>';
			}
			
			// identification title tags
			$increase_btn_title = $this->get_text($this->params->get( 'increase_font_size_title' ), 'INCREASE_FS_TITLE');
			$decreace_btn_title = $this->get_text($this->params->get( 'decrease_font_size_title' ), 'DECREASE_FS_TITLE');
			$reset_btn_title = $this->get_text($this->params->get( 'reset_font_size_title' ), 'RESET_FS_TITLE');

			// define css class for button with images
			if ( $this->params->get( 'use_pictures_for_links' ) ) {
				$img_button = ' '.$this->params->get( 'default_image_button_class' );
			} else {
				$img_button = '';
			}
			
			// add increase button
			$html[] = '<li class="first-child">';
			$html[] = '<a href="#" title="'.$increase_btn_title.'" onclick="changeFontSize(2); return false" class="'.$this->params->get( 'increase_button_class' ).$img_button.'">';
			if ( !$this->params->get( 'use_pictures_for_links' ) ) {
				$html[] =  $this->get_text($this->params->get( 'increase_font_size_text' ), 'INCREASE_FS');
			}
			$html[] = '</a>';
			$html[] = '</li>';

			// add reset button
			if ( $this->params->get( 'show_reset_button' ) ) {
				$html[] = '<li class="'.$this->params->get( 'default_separator' ).'">';
				$html[] = '<a href="#" title="'.$reset_btn_title.'" onclick="revertStyles(); return false" class="'.$this->params->get( 'reset_button_class' ).$img_button.'">';
				if ( !$this->params->get( 'use_pictures_for_links' ) ) {
					$html[] =  $this->get_text($this->params->get( 'reset_font_size_text' ), 'RESET_FS');
				}
				$html[] = '</a>';
				$html[] = '</li>';
			}
			
			// add decrease button
			$html[] = '<li class="'.$this->params->get( 'default_separator' ).'">';
			$html[] = '<a href="#" title="'.$decreace_btn_title.'" onclick="changeFontSize(-2); return false" class="'.$this->params->get( 'decrease_button_class' ).$img_button.'">';
			if ( !$this->params->get( 'use_pictures_for_links' ) ) {
				$html[] =  $this->get_text($this->params->get( 'decrease_font_size_text' ), 'DECREASE_FS');
			}
			$html[] = '</a>';
			$html[] = '</li>';
			
			// end list
			$html[] = '</ul>';
			
			$html = implode('', $html);
			
			$row->text =  str_replace($matches[0][0], $html, $row->text);
			
			// add css and js files in head
			$doc = JFactory::getDocument();
			
			// load mootools first
			if (file_exists(JPATH_BASE."/media/system/js/mootools-core.js")){
				JHtml::_('behavior.framework');
			}
			
			$doc->addScript( JURI::base().'plugins/system/evs_fontsize_switcher/fontsize.js' );

			$custom_css = $this->params->get( 'custom_css' );
				
			if (empty($custom_css)) {
				$doc->addStyleSheet( JURI::base().'plugins/system/evs_fontsize_switcher/fontsize.css' );
			} else {
				$doc->addStyleDeclaration( $custom_css );
			}
			
		}
		
		return '';

	}
	
	public function get_text($text = '', $lang_constant) {
		
		if ( empty($text) ){
			return JText::_( 'PLG_SYSTEM_EVS_FONTSIZE_SWITCHER_'.$lang_constant );
		} else {
			return $text;
		}
		
	}

}
